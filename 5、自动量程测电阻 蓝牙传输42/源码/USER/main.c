#include "sys.h"

#include "delay.h"
#include "led.h"
#include "usart.h"
#include "ADC.h"

#define PC14_OD_680k()  /*开漏模式*/          {	GPIOC->CRH&=0XF0FFFFFF;	GPIOC->CRH|=0X07000000;			GPIOC->ODR|=1<<14; }
#define PC14_OUT1_680k()  /*输出高电平*/  {	GPIOC->CRH&=0XF0FFFFFF;	GPIOC->CRH|=0X03000000;			GPIOC->ODR|=1<<14; }
#define PC15_OD_100k()  /*开漏模式*/          {	GPIOC->CRH&=0X0FFFFFFF;	GPIOC->CRH|=0X70000000;			GPIOC->ODR|=1<<15; }
#define PC15_OUT1_100k()  /*输出高电平*/  {	GPIOC->CRH&=0X0FFFFFFF;	GPIOC->CRH|=0X30000000;			GPIOC->ODR|=1<<15; }

#define PB10_OD_10k()  /*开漏模式*/         {	GPIOB->CRH&=0XFFFFF0FF;	GPIOB->CRH|=0X00000700;			GPIOB->ODR|=1<<10; }
#define PB10_OUT1_10k()  /*输出高电平*/ {	GPIOB->CRH&=0XFFFFF0FF;	GPIOB->CRH|=0X00000300;			GPIOB->ODR|=1<<10; }
#define PB11_OD_1k()  /*开漏模式*/          {	GPIOB->CRH&=0XFFFF0FFF;	GPIOB->CRH|=0X00007000;			GPIOB->ODR|=1<<10; }
#define PB11_OUT1_1k()  /*输出高电平*/  {	GPIOB->CRH&=0XFFFF0FFF;	GPIOB->CRH|=0X00003000;			GPIOB->ODR|=1<<10; }

u8 flag=0;
u8 speed=20;
void Switch_GPIO_Config(void)
{
	
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE); // ??PC????  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15|GPIO_Pin_14;	//???????
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;       
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);  //???PC??
  GPIO_SetBits(GPIOC, GPIO_Pin_14 );	 // ????LED
  GPIO_SetBits(GPIOC, GPIO_Pin_15 );	 // ????LED

  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE); // ??PC????  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10|GPIO_Pin_11;	//???????
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;       
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);  //???PC??
  GPIO_SetBits(GPIOB, GPIO_Pin_10 );	 // ????LED
  GPIO_SetBits(GPIOB, GPIO_Pin_11 );	 // ????LED
}


int	main()
{
	u32 adc_num;
	u16 i=0;
	u32 show_value=0;
	SystemInit();	// 配置系统时钟为72M 	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable , ENABLE);   //完全禁用JTAG
	
	delay_init();
	LED_GPIO_Config();	
	Switch_GPIO_Config();
	USART1_Config(115200);
	Adc_Init();

	while(1)
	{
		/*
		delay_ms(300);
		GPIO_SetBits(GPIOC, GPIO_Pin_13 );
				delay_ms(300);
		GPIO_ResetBits(GPIOC, GPIO_Pin_13 );
		*/
		if(flag>=1){
		USART_Send_string(USART1,"\r\n");
		delay_ms(2);
			
			if(flag==1){
			
		PC14_OD_680k();   
		PC15_OUT1_100k(); 		
PB10_OD_10k(); 
PB11_OD_1k(); delay_ms(2);
				
				show_value=0;
				for(i=0;i<speed;i++){
					adc_num=Get_Adc(ADC_CH0);     //对应PA0口   改成其他的需要改Adc_Init();
					show_value+=adc_num;
					delay_us(200);
				}
				show_value=show_value/(float)speed;
				
		show_value=99988*show_value/(4095-show_value);    
		USART_Send_Char(USART1,show_value/100000%10+48);
		USART_Send_Char(USART1,show_value/10000%10+48);
		USART_Send_Char(USART1,show_value/1000%10+48);
		USART_Send_Char(USART1,show_value/100%10+48);
		USART_Send_Char(USART1,show_value/10%10+48);
		USART_Send_Char(USART1,show_value%10+48);
			
			
			}else{
			
			
			

	
		PC14_OUT1_680k();   
		PC15_OD_100k(); 		
		PB10_OD_10k(); 
		PB11_OD_1k();   		delay_ms(2);

				show_value=0;
				for(i=0;i<speed;i++){
					adc_num=Get_Adc(ADC_CH0);     //对应PA0口   改成其他的需要改Adc_Init();
					show_value+=adc_num;
					delay_us(200);
				}
				show_value=show_value/(float)speed;
				
				
		show_value=688000.0*show_value/(4095-show_value);      //100k的时候
		if(show_value>500000){
		USART_Send_Char(USART1,show_value/100000%10+48);
		USART_Send_Char(USART1,show_value/10000%10+48);
		USART_Send_Char(USART1,show_value/1000%10+48);
		USART_Send_Char(USART1,show_value/100%10+48);
		USART_Send_Char(USART1,show_value/10%10+48);
		USART_Send_Char(USART1,show_value%10+48);
		}
		else if(show_value>33000){

		PC14_OD_680k();   
		PC15_OUT1_100k(); 		
PB10_OD_10k(); 
PB11_OD_1k(); delay_ms(2);
			
				show_value=0;
				for(i=0;i<speed;i++){
					adc_num=Get_Adc(ADC_CH0);     //对应PA0口   改成其他的需要改Adc_Init();
					show_value+=adc_num;
					delay_us(200);
				}
				show_value=show_value/(float)speed;
			
		show_value=99990*show_value/(4095-show_value);    
		USART_Send_Char(USART1,show_value/100000%10+48);
		USART_Send_Char(USART1,show_value/10000%10+48);
		USART_Send_Char(USART1,show_value/1000%10+48);
		USART_Send_Char(USART1,show_value/100%10+48);
		USART_Send_Char(USART1,show_value/10%10+48);
		USART_Send_Char(USART1,show_value%10+48);
		}
		else if(show_value>3300){
		
		PC14_OD_680k();   
		PC15_OD_100k(); 		
PB10_OUT1_10k(); 
PB11_OD_1k(); delay_ms(2);
			
				show_value=0;
				for(i=0;i<speed;i++){
					adc_num=Get_Adc(ADC_CH0);     //对应PA0口   改成其他的需要改Adc_Init();
					show_value+=adc_num;
					delay_us(200);
				}
				show_value=show_value/(float)speed;
			
		show_value=9991.0*show_value/(float)(4095-show_value);    
		USART_Send_Char(USART1,show_value/100000%10+48);
		USART_Send_Char(USART1,show_value/10000%10+48);
		USART_Send_Char(USART1,show_value/1000%10+48);
		USART_Send_Char(USART1,show_value/100%10+48);
		USART_Send_Char(USART1,show_value/10%10+48);
		USART_Send_Char(USART1,show_value%10+48);
		}
		else{

		PC14_OD_680k();   
		PC15_OD_100k(); 		
PB10_OD_10k(); 
PB11_OUT1_1k(); delay_ms(2);
			
				show_value=0;
				for(i=0;i<speed;i++){
					adc_num=Get_Adc(ADC_CH0);     //对应PA0口   改成其他的需要改Adc_Init();
					show_value+=adc_num;
					delay_us(200);
				}
				show_value=show_value/(float)speed;
				
		show_value=1024.0*show_value/(float)(4095-show_value);    
		USART_Send_Char(USART1,show_value/100000%10+48);
		USART_Send_Char(USART1,show_value/10000%10+48);
		USART_Send_Char(USART1,show_value/1000%10+48);
		USART_Send_Char(USART1,show_value/100%10+48);
		USART_Send_Char(USART1,show_value/10%10+48);
		USART_Send_Char(USART1,show_value%10+48);
		}
		}
	}
	}

}

void USART1_IRQHandler(void)                	//串口1中断服务程序
	{
	u8 Res;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  //接收中断(接收到的数据必须是0x0d 0x0a结尾)
		{
		Res =USART_ReceiveData(USART1);	//读取接收到的数据
if(Res=='s')  {			flag=0; 		GPIO_SetBits(GPIOC, GPIO_Pin_13 ); }  //停止
if(Res=='1')  {			flag=1;		GPIO_ResetBits(GPIOC, GPIO_Pin_13 );  }  //大量程   精度低
if(Res=='2')  {			flag=2; 		GPIO_ResetBits(GPIOC, GPIO_Pin_13 );}   //自动分档   精度高


if(Res=='u'){
if(speed<90)	speed++;	else speed=1;
		USART_Send_Char(USART1,'[');
		USART_Send_Char(USART1,speed/10%10+48);
		USART_Send_Char(USART1,speed%10+48);
			USART_Send_Char(USART1,']');
}
if(Res=='d'){
if(speed>1)	speed--;	else speed=90;
			USART_Send_Char(USART1,'[');
		USART_Send_Char(USART1,speed/10%10+48);
		USART_Send_Char(USART1,speed%10+48);
				USART_Send_Char(USART1,']');
}




     } 

} 
