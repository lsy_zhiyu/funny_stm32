#include "sys.h"

#include "delay.h"
#include "led.h"
#include "usart.h"

#include "iic.h"
#include "oled_iic.h"
#include "word.h"
#include "ds1302.h"

//#define FLASH_ADR 0X0800FC00  //???c8t6?flash??????    ??????

u8 RXD_num = 0;          //串口读取的字符数  用于判断长命令的
u8 uart_flag = 0;          //串口读取长数据 是否完成
u8 uart_data[20];          //串口数据存放的数组
u16 todayin_num = 0;
u16 todayout_num = 0;
u8 today = 0;

u16 data[3 * 7] = {1, 23, 33, 2, 54, 56, 3, 89, 96, 4, 128, 210, 5, 234, 345, 6, 32, 80, 7, 34, 22}; //预先存入的数据

#define LED PCout(13)      //led 板子上自带的
#define Count_in GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6)            //光电模块   进入口
#define Count_out GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)           //光电模块   出口
#define key GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_15)                   //按键

void Other_Gpio_Init(void)     // 按键    红外
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

  //按键
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;//上拉输入
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  //红外入
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;//上拉输入
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  //红外出
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;//上拉输入
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void OLED_DisplayNum(u8 i, u8 j, u8 k, u8 l, u16 num) { //oled 在位置显示  数字
  u8 temp[5];
  //temp[0]=num/10000%10+48;
  temp[0] = num / 1000 % 10 + 48;
  temp[1] = num / 100 % 10 + 48;
  temp[2] = num / 10 % 10 + 48;
  temp[3] = num % 10 + 48;
  temp[4] = '\0';
  OLED_DisplayString(i, j, k, l, temp);
}





int main()
{
  u8 i;
  u8 Count_in_last = 0;   //进入的 光电模块上一次状态     只有高电平变低电平  计数一次    ，持续触发无效
  u8 Count_out_last = 0; //出口的 光电模块上一次状态     只有高电平变低电平  计数一次    ，持续触发无效
  u16 times = 0;        //主函数执行次数
  u8 Show_temp[17];   //用于oled显示缓存
  u8 time[9];              //用于显示时间
  SystemInit(); // 配置系统时钟为72M
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable , ENABLE);   //完全禁用JTAG

  delay_init();           //延时 初始化
  LED_GPIO_Config();   //led引脚初始化
  USART1_Config(9600);    //串口1 初始化
  //USART2_Config(9600);
  //USART3_Config(9600);

  OLED_Init();  //OLED初始化
  Ds1302_Gpio_Init();    //ds1302初始化
  Other_Gpio_Init();    //按键  光电 引脚初始化

  Ds1302_Readtime();      //读取实时时间
  today = ds1302Data.day; //开机获取当前日期

  while (1)
  {
    times++;

    if (uart_flag == 1) {  //长命令接收完成时  用于处理串口1发来的命令      注意！！stlink连接时有时获取时间不对，需要单独另外供电
      if (uart_data[14] == '*') {      //串口发送数据  修改ds1302的实时时间     比如#2006041210104*   最后是星期几

        ds1302Data.year = (uart_data[1] - 48) * 10 + uart_data[2] - 48;             //修改时间
        ds1302Data.month = (uart_data[3] - 48) * 10 + uart_data[4] - 48;
        ds1302Data.day = (uart_data[5] - 48) * 10 + uart_data[6] - 48;
        ds1302Data.hour = (uart_data[7] - 48) * 10 + uart_data[8] - 48;
        ds1302Data.min = (uart_data[9] - 48) * 10 + uart_data[10] - 48;
        ds1302Data.sec = (uart_data[11] - 48) * 10 + uart_data[12] - 48;
        ds1302Data.week = uart_data[13] - 48;
        Ds1302_Write_Time_All(1);
        USART_Send_string(USART1, "\r\nchange realtime ok!\r\n");
      }
      for (i = 0; i < 20; i++) uart_data[i] = 0;   //清空数组
      uart_flag = 0;
      delay_s(1);
    }

    if (Count_in_last == 1)  //如果光电门  上一次状态时高电平
      if (Count_in == 0) {   //本次低电平
        delay_ms(15);
        if (Count_in == 0) {
          todayin_num++;         //计数+1
          delay_ms(15);
        }
      }
    Count_in_last = Count_in; //更新最后一次状态
    if (Count_out_last == 1) //同理
      if (Count_out == 0) {
        delay_ms(15);
        if (Count_out == 0) {
          todayout_num++;
          delay_ms(15);
        }
      }
    Count_out_last = Count_out;

    if (key == 0) {   //如果按键按下
      delay_ms(15);
      if (key == 0) {

        Show_temp[0] = 'd';    //oled显示内容缓存
        Show_temp[1] = 'a';
        Show_temp[2] = 'y';
        Show_temp[3] = ':';
        Show_temp[6] = ' ';
        Show_temp[11] = ' ';
        Show_temp[16] = '\0';

        Show_temp[4] = data[18] / 10 % 10 + 48; //一下依次显示  之前一周的数据， 天数  进入人数  出去人数
        Show_temp[5] = data[18] % 10 + 48;
        i = data[19];
        Show_temp[7] = i / 1000 % 10 + 48;
        Show_temp[8] = i / 100 % 10 + 48;
        Show_temp[9] = i / 10 % 10 + 48;
        Show_temp[10] = i % 10 + 48;
        i = data[20];
        Show_temp[12] = i / 1000 % 10 + 48;
        Show_temp[13] = i / 100 % 10 + 48;
        Show_temp[14] = i / 10 % 10 + 48;
        Show_temp[15] = i % 10 + 48;

        OLED_DisplayString(8 * 0, 6, 16, 16, Show_temp);

        Show_temp[4] = data[15] / 10 % 10 + 48;
        Show_temp[5] = data[15] % 10 + 48;
        i = data[16];
        Show_temp[7] = i / 1000 % 10 + 48;
        Show_temp[8] = i / 100 % 10 + 48;
        Show_temp[9] = i / 10 % 10 + 48;
        Show_temp[10] = i % 10 + 48;
        i = data[17];
        Show_temp[12] = i / 1000 % 10 + 48;
        Show_temp[13] = i / 100 % 10 + 48;
        Show_temp[14] = i / 10 % 10 + 48;
        Show_temp[15] = i % 10 + 48;
        OLED_DisplayString(8 * 0, 4, 16, 16, Show_temp);

        Show_temp[4] = data[12] / 10 % 10 + 48;
        Show_temp[5] = data[12] % 10 + 48;
        i = data[13];
        Show_temp[7] = i / 1000 % 10 + 48;
        Show_temp[8] = i / 100 % 10 + 48;
        Show_temp[9] = i / 10 % 10 + 48;
        Show_temp[10] = i % 10 + 48;
        i = data[14];
        Show_temp[12] = i / 1000 % 10 + 48;
        Show_temp[13] = i / 100 % 10 + 48;
        Show_temp[14] = i / 10 % 10 + 48;
        Show_temp[15] = i % 10 + 48;
        OLED_DisplayString(8 * 0, 2, 16, 16, Show_temp);

        Show_temp[4] = data[9] / 10 % 10 + 48;
        Show_temp[5] = data[9] % 10 + 48;
        i = data[10];
        Show_temp[7] = i / 1000 % 10 + 48;
        Show_temp[8] = i / 100 % 10 + 48;
        Show_temp[9] = i / 10 % 10 + 48;
        Show_temp[10] = i % 10 + 48;
        i = data[11];
        Show_temp[12] = i / 1000 % 10 + 48;
        Show_temp[13] = i / 100 % 10 + 48;
        Show_temp[14] = i / 10 % 10 + 48;
        Show_temp[15] = i % 10 + 48;
        OLED_DisplayString(8 * 0, 0, 16, 16, Show_temp);

        delay_s(3);

        Show_temp[4] = data[6] / 10 % 10 + 48;
        Show_temp[5] = data[6] % 10 + 48;
        i = data[7];
        Show_temp[7] = i / 1000 % 10 + 48;
        Show_temp[8] = i / 100 % 10 + 48;
        Show_temp[9] = i / 10 % 10 + 48;
        Show_temp[10] = i % 10 + 48;
        i = data[8];
        Show_temp[12] = i / 1000 % 10 + 48;
        Show_temp[13] = i / 100 % 10 + 48;
        Show_temp[14] = i / 10 % 10 + 48;
        Show_temp[15] = i % 10 + 48;

        OLED_DisplayString(8 * 0, 6, 16, 16, Show_temp);

        Show_temp[4] = data[3] / 10 % 10 + 48;
        Show_temp[5] = data[3] % 10 + 48;
        i = data[4];
        Show_temp[7] = i / 1000 % 10 + 48;
        Show_temp[8] = i / 100 % 10 + 48;
        Show_temp[9] = i / 10 % 10 + 48;
        Show_temp[10] = i % 10 + 48;
        i = data[5];
        Show_temp[12] = i / 1000 % 10 + 48;
        Show_temp[13] = i / 100 % 10 + 48;
        Show_temp[14] = i / 10 % 10 + 48;
        Show_temp[15] = i % 10 + 48;
        OLED_DisplayString(8 * 0, 4, 16, 16, Show_temp);

        Show_temp[4] = data[0] / 10 % 10 + 48;
        Show_temp[5] = data[0] % 10 + 48;
        i = data[1];
        Show_temp[7] = i / 1000 % 10 + 48;
        Show_temp[8] = i / 100 % 10 + 48;
        Show_temp[9] = i / 10 % 10 + 48;
        Show_temp[10] = i % 10 + 48;
        i = data[2];
        Show_temp[12] = i / 1000 % 10 + 48;
        Show_temp[13] = i / 100 % 10 + 48;
        Show_temp[14] = i / 10 % 10 + 48;
        Show_temp[15] = i % 10 + 48;
        OLED_DisplayString(8 * 0, 2, 16, 16, Show_temp);
        OLED_DisplayString(0, 0, 16, 16, "                ");

        delay_s(3);

      }
    }
    delay_ms(10);


    if (times >= 100) { //运行一定次数后   更新oled显示
      times = 0;
      LED = 1 - LED; //板子led闪烁
      OLED_DisplayString(0, 0, 16, 16, "Stm32  hello    "); //显示字符串   、实时年月日   时分秒
      Ds1302_Readtime();
      time[0] = ds1302Data.year / 10 + 48;
      time[1] = ds1302Data.year % 10 + 48;
      time[2] = '-';
      time[3] = ds1302Data.month / 10 + 48;
      time[4] = ds1302Data.month % 10 + 48;
      time[5] = '-';
      time[6] = ds1302Data.day / 10 + 48;
      time[7] = ds1302Data.day % 10 + 48;
      time[8] = '\0';
      OLED_DisplayString(0, 2, 16, 16, time);
      OLED_DisplayString(8 * 8, 2, 16, 16, "        ");
      time[0] = ds1302Data.hour / 10 + 48;
      time[1] = ds1302Data.hour % 10 + 48;
      time[2] = ':';
      time[3] = ds1302Data.min / 10 + 48;
      time[4] = ds1302Data.min % 10 + 48;
      time[5] = ':';
      time[6] = ds1302Data.sec / 10 + 48;
      time[7] = ds1302Data.sec % 10 + 48;
      time[8] = '\0';
      OLED_DisplayString(0, 4, 16, 16, time);
      OLED_DisplayString(8 * 8, 4, 16, 16, "        ");

      OLED_DisplayString(0, 6, 16, 16, "in:");        //显示今天出入人数
      OLED_DisplayNum(8 * 3, 6, 16, 16, todayin_num);
      OLED_DisplayString(8 * 7, 6, 16, 16, " ");
      OLED_DisplayString(8 * 8, 6, 16, 16, "out:");
      OLED_DisplayNum(8 * 12, 6, 16, 16, todayout_num);

      if (today != ds1302Data.day) { //过第二天的标志   存贮数据
        for (i = 0; i < 6; i++) {        //数据搬移
          data[0 + i * 3] = data[3 + i * 3];
          data[1 + i * 3] = data[4 + i * 3];
          data[2 + i * 3] = data[5 + i * 3];
        }
        today = ds1302Data.day; //更新  新的一天的数据
        todayin_num = 0;
        todayout_num = 0;
        data[18] = today;
        data[19] = todayin_num;
        data[20] = todayout_num;
      }


      if (ds1302Data.sec % 60 == 0) { //最新的   今天的   每分钟存一次    0秒的时候
        data[18] = today;
        data[19] = todayin_num;
        data[20] = todayout_num;
      }


    }


  }
}

void USART1_IRQHandler(void)                  //串口1中断服务程序
{
  u8 Res;
  if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) //接收中断(接收到的数据必须是0x0d 0x0a结尾)
  {
    Res = USART_ReceiveData(USART1); //读取接收到的数据

    if (Res == '#')  RXD_num = 0;   //开始符
    uart_data[RXD_num] = Res;
    RXD_num++;
    if (Res == '*') {     //结束符
      RXD_num = 0;
      uart_flag = 1;      //完成 长数据的接收

      USART_Send_string(USART1, "get:");
      USART_Send_string(USART1, uart_data);   //把收到的数据发回去
    }


  }
}





