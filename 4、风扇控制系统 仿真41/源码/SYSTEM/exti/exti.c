#include "stm32f10x.h"
#include "exti.h"

uint8_t EXTI_Status = 0;

void ExtiInit(void)
{
	EXTI_InitTypeDef EXTI_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_AFIO,ENABLE);

	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource0);//配置中断线A-0
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource1);//配置中断线A-8
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource2);//配置中断线B-1
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource3);//配置中断线B-2
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource4);//配置中断线B-1
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource5);//配置中断线B-2
	
	EXTI_InitStructure.EXTI_Line=EXTI_Line0; //配置中断线A-0
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	EXTI_InitStructure.EXTI_Line=EXTI_Line1; //配置中断线A-8
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	EXTI_InitStructure.EXTI_Line=EXTI_Line2; //配置中断线B-1
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	EXTI_InitStructure.EXTI_Line=EXTI_Line3; //配置中断线B-2
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	EXTI_InitStructure.EXTI_Line=EXTI_Line4; //配置中断线B-1
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	EXTI_InitStructure.EXTI_Line=EXTI_Line5; //配置中断线B-2
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5;//按键配置
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init(GPIOC,&GPIO_InitStructure);
	
	
	NVIC_InitStructure.NVIC_IRQChannel=EXTI0_IRQn; //配置中断A-0
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=2;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	

	NVIC_InitStructure.NVIC_IRQChannel=EXTI1_IRQn;//配置中断A-8
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	

	NVIC_InitStructure.NVIC_IRQChannel=EXTI2_IRQn;//配置中断B-1
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	

	NVIC_InitStructure.NVIC_IRQChannel=EXTI3_IRQn; //配置中断B-2
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=3;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	
			
	NVIC_InitStructure.NVIC_IRQChannel=EXTI4_IRQn;//配置中断B-1
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	

	NVIC_InitStructure.NVIC_IRQChannel=EXTI9_5_IRQn; //配置中断B-2
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	
			
}



void EXTI0_IRQHandler(void)	//A-0 中断函数
{

	if(EXTI_GetITStatus(EXTI_Line0)!=0)
	{
		EXTI_Status=0;
		EXTI_ClearITPendingBit(EXTI_Line0);//清除LINE上的中断标志位

	}
}
void EXTI1_IRQHandler(void)	//A-0 中断函数
{

	if(EXTI_GetITStatus(EXTI_Line1)!=0)
	{
		EXTI_Status=1;
		EXTI_ClearITPendingBit(EXTI_Line1);//清除LINE上的中断标志位

	}
}
void EXTI2_IRQHandler(void)	//A-0 中断函数
{

	if(EXTI_GetITStatus(EXTI_Line2)!=0)
	{
		EXTI_Status=2;
		EXTI_ClearITPendingBit(EXTI_Line2);//清除LINE上的中断标志位

	}
}
void EXTI3_IRQHandler(void)	//A-0 中断函数
{

	if(EXTI_GetITStatus(EXTI_Line3)!=0)
	{
		EXTI_Status=3;
		EXTI_ClearITPendingBit(EXTI_Line3);//清除LINE上的中断标志位

	}
}

void EXTI9_5_IRQHandler(void)	//A-8 中断函数
{
	if(EXTI_GetITStatus(EXTI_Line5)!=0)
	{
		EXTI_Status=5;
		EXTI_ClearITPendingBit(EXTI_Line5);//清除LINE上的中断标志位
	}
}


void EXTI4_IRQHandler(void)	//B-1 中断函数
{
	if(EXTI_GetITStatus(EXTI_Line4)!=0)
	{
		EXTI_Status=4;
		EXTI_ClearITPendingBit(EXTI_Line4);//清除LINE上的中断标志位
	}
}



