#include "stm32f10x.h"
#include "bsp-lcd1602.h"
#include "delay.h"
#include "sys.h"
#include "adc.h"
#include "exti.h"
void LED_GPIO_Config(void)      //设置4个led 的引脚  为输出模式
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE); // ??PC????
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_12 | GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9; //???????
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);  //???PC??

  GPIO_SetBits(GPIOC, GPIO_Pin_13 | GPIO_Pin_12 | GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9); // ????LED
}
void KEY_GPIO_Config(void)  //设置6个按键   为上拉输入模式
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE); // ??PC????
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5; //???????
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);  //???PC??
  GPIO_SetBits(GPIOC, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5);
}
void io_config(void)    //设置l298的  电机方向控制引脚  输出模式
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE); // ??PC????
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7; //???????
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);  //???PC??
  GPIO_ResetBits(GPIOA, GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 ); // ????LED
}

void TIM2_Configuration( uint16_t _period )    //设置定时器2  为pwm模式，使用了通道3和4
{
  TIM_TimeBaseInitTypeDef   TIM_TimeBaseStructure;
  GPIO_InitTypeDef          GPIO_InitStructure;
  TIM_OCInitTypeDef         TIM_OCInitStructure;

  //输出比较通道 GPIO 初始化
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA  , ENABLE );
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  //使能时钟
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_TIM1 , ENABLE ); //仿真TIM2必须使能TIM1时钟，否则出错
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2 , ENABLE );
  // 基本定时器配置
  TIM_TimeBaseStructure.TIM_Period = _period - 1;
  TIM_TimeBaseStructure.TIM_Prescaler = 7;
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;// 时钟分频因子
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;// 计数器计数模式，设置为向上计数
  TIM_TimeBaseInit( TIM2 , &TIM_TimeBaseStructure);
  // 配置为PWM模式
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;             // 小于门限有效
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; // 使能输出
  TIM_OCInitStructure.TIM_Pulse = (_period - 1) / 2;            // 设置占空比大小
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;     // 有效电平为高
  TIM_OC1Init( TIM2 , &TIM_OCInitStructure );
  //  TIM_OC2Init( TIM2 , &TIM_OCInitStructure );
  TIM_OC3Init( TIM2 , &TIM_OCInitStructure );
  TIM_OC4Init( TIM2 , &TIM_OCInitStructure );
  //使能TIM2 在 CCR1,2,3,4 上的预装载寄存器,否则只执行一次
  TIM_OC1PreloadConfig( TIM2 , TIM_OCPreload_Enable );
  //  TIM_OC2PreloadConfig( TIM2 , TIM_OCPreload_Enable );
  TIM_OC3PreloadConfig( TIM2 , TIM_OCPreload_Enable );
  TIM_OC4PreloadConfig( TIM2 , TIM_OCPreload_Enable );

  //使能中断
  //  TIM_ITConfig(TIM2, TIM_IT_CC1, ENABLE);

  //使能定时器
  TIM_Cmd( TIM2 , ENABLE );

  //  TIM2_SetPWMPuls( 1,0 );
  //  TIM2_SetPWMPuls( 2,0 );
  //  TIM2_SetPWMPuls( 3,0 );
  //  TIM2_SetPWMPuls( 4,0 );
  //  TIM2_PWMPeriod = _period;
}
/******************************************************************
  定时器3 PWM脉冲配置函数，设置高脉冲持续时间
  参数：_ch:输出通道号:1,2,3,4
      _puls:高脉冲持续时间,单位us,不得超过配置函数中设置的PWM周期时间
******************************************************************/
void TIM2_SetPWMPuls(uint8_t _ch, uint16_t _puls )   //设置定时器2的各个通道的占空比
{
  //if( _puls>TIM2_PWMPeriod )  return;     //5000？

  switch ( _ch )
  {
    case 1: TIM2->CCR1 = _puls; break;
    case 2: TIM2->CCR2 = _puls; break;
    case 3: TIM2->CCR3 = _puls; break;
    case 4: TIM2->CCR4 = _puls; break;
    default: break;
  }
}
extern uint8_t EXTI_Status;
int main(void)
{
  int b, c, d;
  float temp;       //用于获取电压
  u32 run_time = 0;    //主函数运行次数
  int dingshi_num = 0;  //定时有关的变量
  int now_temp = 0;    //此时的温度
  u8 yao_flag = 0;     //是否摇头的  变量标志
  LED_GPIO_Config();     //led初始化
  KEY_GPIO_Config();     //按键初始化
  delay_init();        //延时函数初始化
  TIM2_Configuration(5000);   //定时器2pwm初始化
  io_config();             //l298初始化
  ExtiInit();                //外部中断初始化
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //NVIC设置，有关中断的
  LCD1602_Init();        //lcd1602初始化
  ADC1_GPIO_Config();   //adc引脚初始化
  ADC_Config();            //adc功能初始化
  //  LCD1602_ShowStr(2,0,"adcvalue=0.00V",14);
  LCD1602_ShowStr(0, 1, "temp:    C  36C", 15);    //第二行显示当前温度和电机警报温度

  while (1)
  {
    if (dingshi_num != 0)  run_time++;   //定时功能开启  才会计算
    //LCD1602_ShowStr(0,1,"temp:    C  36C",15);
    delay_ms(20);
    b = ADC_GetConversionValue(ADC1);  //获取adc值
    temp = (float)b * (5.0 / 4096);
    c = temp * 10;
    d = c % 10;
    now_temp = d * 10;
    LCD_ShowNum(6, 1, d);
    c = temp * 100;
    d = c % 10;
    LCD_ShowNum(7, 1, d);    //转化成温度   显示在lcd1602上

    now_temp += d;
    if (now_temp > 36) {  //超过温度，停风扇
      GPIO_ResetBits(GPIOA, GPIO_Pin_6 );
      GPIO_ResetBits(GPIOA, GPIO_Pin_7);
      LCD1602_ShowStr(0, 0, "8,temp high     ", 16);
      PCout(13) = 0;
      EXTI_Status = 7;
    }
    else {        //否则  正常 转       
      PCout(13) = 1;
      GPIO_ResetBits(GPIOA, GPIO_Pin_6 );
      GPIO_SetBits(GPIOA, GPIO_Pin_7);
      LCD1602_ShowStr(0, 0, "                ", 16);
    }



    if (EXTI_Status == 4)
    { //自然风
      GPIO_SetBits(GPIOC, GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 );
      GPIO_ResetBits(GPIOC, GPIO_Pin_12 );   //亮灯
      TIM2_SetPWMPuls(4, 2500);    //占空比较高  速度较快
      LCD1602_ShowStr(0, 0, "1,ziran         ", 16);    //显示状态
      delay_ms(20);
    }
    if (EXTI_Status == 3)
    { //常风
      GPIO_SetBits(GPIOC, GPIO_Pin_12 | GPIO_Pin_10 | GPIO_Pin_9 );
      GPIO_ResetBits(GPIOC, GPIO_Pin_11 );   //亮
      TIM2_SetPWMPuls(4, 3500);

      LCD1602_ShowStr(0, 0, "2,changfeng     ", 16);
      delay_ms(20);
    }
    if (EXTI_Status == 2)
    { //睡眠风
      GPIO_SetBits(GPIOC, GPIO_Pin_12 | GPIO_Pin_11 | GPIO_Pin_9 );
      GPIO_ResetBits(GPIOC, GPIO_Pin_10 );   //亮
      TIM2_SetPWMPuls(4, 1500);
      LCD1602_ShowStr(0, 0, "3,shuimian      ", 16);
      delay_ms(20);
    }
    if (EXTI_Status == 5)
    { //无风
      GPIO_SetBits(GPIOC, GPIO_Pin_12 | GPIO_Pin_11 | GPIO_Pin_10 );
      GPIO_ResetBits(GPIOC, GPIO_Pin_9 );   //亮
      TIM2_SetPWMPuls(4, 0);     //关闭

      LCD1602_ShowStr(0, 0, "4,wufeng 123       ", 16);
      delay_ms(20);
    }
    if (EXTI_Status == 1)
    { //定时
      dingshi_num++;    //每按一次   增加5s
      run_time = 0;
      LCD1602_ShowStr(0, 0, "5,dingshi  +5s  ", 16);
      delay_ms(20);
    }
    //if(run_time/3000/10>dingshi_num){   //有关定时的
    if (run_time / 50 / 5 > dingshi_num) { //有关定时的     超过时间久  停风扇
      dingshi_num = 0;
      run_time = 0;
      GPIO_SetBits(GPIOC, GPIO_Pin_12 | GPIO_Pin_11 | GPIO_Pin_10 );
      GPIO_ResetBits(GPIOC, GPIO_Pin_9 );   //亮
      TIM2_SetPWMPuls(4, 0);

      LCD1602_ShowStr(0, 0, "7,timeout       ", 16);
    }
    if (EXTI_Status == 0)   //是否摇头
    { //摇头
      yao_flag = 1 - yao_flag;
      if (yao_flag == 1) {
        TIM2_SetPWMPuls(3, 1000);
        GPIO_SetBits(GPIOC, GPIO_Pin_4 );
        GPIO_ResetBits(GPIOC, GPIO_Pin_5);
        LCD1602_ShowStr(0, 0, "             yao", 16);
        delay_ms(20);
      }
      else {
        TIM2_SetPWMPuls(3, 0);
        LCD1602_ShowStr(0, 0, "                ", 16);
        delay_ms(20);
      }

    }






  }
}


