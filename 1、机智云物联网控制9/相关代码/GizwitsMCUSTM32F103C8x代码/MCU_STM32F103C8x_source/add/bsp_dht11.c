#include "bsp_dht11.h"

#define Delay_ms(x)  HAL_Delay(x)

static void DHT11_Delay(uint16_t time)
{
  while(time)
  {    
                unsigned char i;
                i = 5;
                while (--i)          {          }
    time--;
  }
}
void DHT11_PIN_MODE(uint8_t mode)                        //???DHT11_DATA PIN
{
        GPIO_InitTypeDef GPIO_InitStruct = {0};
        if(mode==Intput)
        {
                GPIO_InitStruct.Pin = DHT11_DATA_Pin;
                GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
                GPIO_InitStruct.Pull = GPIO_PULLUP;
                GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
                HAL_GPIO_Init(DHT11_DATA_GPIO_Port, &GPIO_InitStruct);
        }
        else
        {
                GPIO_InitStruct.Pin = DHT11_DATA_Pin;
                GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
                GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
                HAL_GPIO_Init(DHT11_DATA_GPIO_Port, &GPIO_InitStruct);        
        }
}
static uint8_t DHT11_ReadByte ( void )
{
        
        DHT11_PIN_MODE(Intput);
        
        uint8_t i, temp=0;
        
        for(i=0;i<8;i++)    
        {         
                /*?bit?50us???????,???????? ?50us ??? ??*/  
                while(Read_DHT11_DATA()==GPIO_PIN_RESET);

                /*DHT11 ?26~28us??????�0�,?70us?????�1�,
                 *???? x us???????????? ,x ?????? 
                 */
                DHT11_Delay(40); //??x us ??????????0???????                     

                if(Read_DHT11_DATA()==GPIO_PIN_SET)/* x us??????????�1� */
                {
                        /* ????1?????? */
                        while(Read_DHT11_DATA()==GPIO_PIN_SET);

                        temp|=(uint8_t)(0x01<<(7-i));  //??7-i??1,MSB?? 
                }
                else         // x us?????????�0�
                {                           
                        temp&=(uint8_t)~(0x01<<(7-i)); //??7-i??0,MSB??
                }
        }
        return temp;
}

uint8_t DHT11_Read_TempAndHumidity(DHT11_Data_TypeDef *DHT11_Data)
{
        uint8_t temp;
  uint16_t humi_temp;

        /*????*/
        DHT11_PIN_MODE(Output);
        /*????*/
        DHT11_DATA_RESET();
        /*??18ms*/
        Delay_ms(18);

        /*???? ????30us*/
        DHT11_DATA_SET(); 

        DHT11_Delay(30);   //??30us

        /*?????? ????????*/ 
        DHT11_PIN_MODE(Intput);

        /*?????????????? ???????,???????*/   
        if(Read_DHT11_DATA()==GPIO_PIN_RESET)     
        {
                /*???????? ?80us ??? ??????*/  
                while(Read_DHT11_DATA()==GPIO_PIN_RESET);

                /*????????? 80us ??? ??????*/
                while(Read_DHT11_DATA()==GPIO_PIN_SET);

                /*??????*/   
                DHT11_Data->humi_high8Bit= DHT11_ReadByte();
                DHT11_Data->humi_low8bit = DHT11_ReadByte();
                DHT11_Data->temp_high8bit= DHT11_ReadByte();
                DHT11_Data->temp_low8bit = DHT11_ReadByte();
                DHT11_Data->check_sum    = DHT11_ReadByte();

                /*????,????????*/
                DHT11_PIN_MODE(Output);
                /*????*/
                DHT11_DATA_SET();

                /* ??????? */
                humi_temp=DHT11_Data->humi_high8Bit*100+DHT11_Data->humi_low8bit;
                DHT11_Data->humidity =(float)humi_temp/100;

                humi_temp=DHT11_Data->temp_high8bit*100+DHT11_Data->temp_low8bit;
                DHT11_Data->temperature=(float)humi_temp/100;    

                /*???????????*/
                temp = DHT11_Data->humi_high8Bit + DHT11_Data->humi_low8bit + 
                DHT11_Data->temp_high8bit+ DHT11_Data->temp_low8bit;
                if(DHT11_Data->check_sum==temp)
                { 
                        return SUCCESS;
                }
                else 
                        return ERROR;
        }
        else
                return ERROR;
}
