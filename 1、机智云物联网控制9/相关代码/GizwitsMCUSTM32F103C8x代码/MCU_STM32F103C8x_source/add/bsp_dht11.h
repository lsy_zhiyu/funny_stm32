#ifndef __DHT11_H
#define __DHT11_H
#include "sys.h"
typedef struct
{
        uint8_t  humi_high8Bit;                       
        uint8_t  humi_low8bit;                       
        uint8_t  temp_high8bit;                 
        uint8_t  temp_low8bit;                        
        uint8_t  check_sum;                 
  float    humidity;                                                            
  float    temperature;                                                         
} DHT11_Data_TypeDef;
enum DHT11_Pin_mode_enum
{
        Intput=1,
        Output,
};
#define DHT11_DATA_GPIO_Port       GPIOA
#define DHT11_DATA_Pin        GPIO_PIN_7

#define Read_DHT11_DATA()         HAL_GPIO_ReadPin(DHT11_DATA_GPIO_Port,DHT11_DATA_Pin)
#define DHT11_DATA_SET()        HAL_GPIO_WritePin(DHT11_DATA_GPIO_Port,DHT11_DATA_Pin,GPIO_PIN_SET)
#define DHT11_DATA_RESET()        HAL_GPIO_WritePin(DHT11_DATA_GPIO_Port,DHT11_DATA_Pin,GPIO_PIN_RESET)

uint8_t DHT11_Read_TempAndHumidity(DHT11_Data_TypeDef *DHT11_Data);

#endif
