/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

#include "hal_key.h"
#include "gizwits_product.h"
#include "common.h"

#include "bsp_dht11.h"
#include "led.h"
#include "lcd.h"
#include "key.h"
#include "buzzer.h"

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
//#define GPIO_KEY_NUM 2 ///< Defines the total number of key member
//keyTypedef_t singleKey[GPIO_KEY_NUM]; ///< Defines a single key member array pointer
keysTypedef_t keys;  
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
/*


    gizwitsSetMode(WIFI_PRODUCTION_TEST);
    gizwitsSetMode(WIFI_RESET_MODE);
    gizwitsSetMode(WIFI_SOFTAP_MODE);
    gizwitsSetMode(WIFI_AIRLINK_MODE);

*/

/**
* Key init function
* @param none
* @return none
*/
/*
void keyInit(void)
{
    singleKey[0] = keyInitOne(NULL, KEY1_GPIO_Port, KEY1_Pin, key1ShortPress, key1LongPress);
    singleKey[1] = keyInitOne(NULL, KEY2_GPIO_Port, KEY2_Pin, key2ShortPress, key2LongPress);
    keys.singleKey = (keyTypedef_t *)&singleKey;
    keyParaInit(&keys); 
}*/
/* USER CODE END 0 */
void delay(u16 k){
  u16 i,j;
	for(i=0;i<k;i++)
	  for(j=0;j<60000;j++);
}



int main(void)
{
  uint32_t i=0;
	u8 key=0;
	u8 hum=0;
	u8 temp=0;
  DHT11_Data_TypeDef  DHT11_Data;
  HAL_Init();             //hal库的初始化
  SystemClock_Config();  //设置系统时钟

	LED_Init();							//初始化LED	
 	LCD_Init();             //LCD屏初始化
	KEY_Init();							//初始化按键
	buzzer_Init();         //蜂鸣器初始化
  MX_TIM2_Init();      //定时器2初始化，为	gizwits 提供1ms的周期时间
  MX_USART1_UART_Init();   //串口1初始化   发送板子状态 ，wifi收发数据状态 给电脑端
  MX_USART2_UART_Init();   //串口2初始化   接wifi模块，进行通讯
  /* Initialize interrupts */
  MX_NVIC_Init();      //中断等级设置
	
	timerInit();            //定时器  关于gizwits定时调用的
	uartInit();              //串口2   关于gizwits接收数据的
	userInit(); 
	gizwitsInit();         //gizwits  初始化
	GIZWITS_LOG("MCU Init Success \n");     //串口1发送 字符串
		DHT11_Read_TempAndHumidity(&DHT11_Data);  //获取温湿度
		delay(20);	
		LCD_Clear(GREEN);   //设置屏幕  绿色背景
		POINT_COLOR=RED;	 //显示字体红色
		LCD_ShowString(30,40,200,24,24,"Welcome ^_^");	  //显示内容
		LCD_ShowString(30,70,200,16,16,"lcd+key+led+buzzer+cloud");  
		LCD_ShowString(30,100,200,16,16,"hum:");
		LCD_ShowString(30,120,200,16,16,"temp:");
		LCD_ShowString(90,100,200,16,16,"%");
		LCD_ShowString(90,120,200,16,16,"C");
		LCD_ShowString(120,290,200,16,16,"beating:");
	  LCD_ShowString(30,150,200,16,16,"notice:");	
		
		i=70;
  while (1)
  {

    key=KEY_Scan(0);				//得到键值
		switch(key)                 
		{				 
			case KEY0_PRES:				LCD_ShowString(90,150,200,16,16,"key0 AIRLINK_MODE   ");    
																					gizwitsSetMode(WIFI_AIRLINK_MODE);	  break;     //一键配置模式
			case KEY1_PRES:				LCD_ShowString(90,150,200,16,16,"key1 SOFTAP_MODE   ");    
																					gizwitsSetMode(WIFI_SOFTAP_MODE);		break;     //热点配网
			case WKUP_PRES:			LCD_ShowString(90,150,200,16,16,"WKUP RESET_MODE   "); 
																					gizwitsSetMode(WIFI_RESET_MODE);		break;       //wifi重启
			default:				break;
		} 
		


		
		
		i++;
		delay(10);	
		if(i%10==0)  LCD_ShowxNum(190,290,i,3,16,0);	
		
		if(i>100){
			i=0;
     DHT11_Read_TempAndHumidity(&DHT11_Data);
     hum	=DHT11_Data.humidity;         
     temp =DHT11_Data.temperature;   
		LCD_ShowxNum(70,100,hum,2,16,0);	
		LCD_ShowxNum(70,120,temp,2,16,0);	
			
			
			if(temp>30){
         	buzzer=0;   //蜂鸣器响   大于50度的时候
				  LCD_ShowString(30,200,200,16,16,"Temp warming!");	 //显示警告
			}
			else{
				  LCD_ShowString(30,200,200,16,16,"               ");	    //清空警告
			}			
			

		userHandle(temp,hum);      //设置上传的数据值
		}
				gizwitsHandle((dataPoint_t *)&currentDataPoint);  //上报数据及其它数据处理
	}


}






/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** NVIC Configuration
*/
static void MX_NVIC_Init(void)
{
  /* TIM2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM2_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(TIM2_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 7200-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 10-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}



/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
