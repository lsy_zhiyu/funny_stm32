#include "sys.h"

#include "delay.h"
#include "led.h"
#include "usart.h"
#include "lcd1602.h"
#include "DS18B20.h"
#include "ADC.h"

u8 get_ip_flag = 0;      //  是否获取到了IP地址
u8 IP_str[16] = "1234567890      ";    //若显示着个，说明esp8266工作不正常  ，显示0.0.0.0  说明没连上wifi
u8 IP_num = 20;  //ip获取过程使用

#define LED PCout(15)      //板 上led
#define LED0 PCout(13)   //最小系统板  上的led
#define BUZZER PCout(14)      //蜂鸣器

void Buzzer_GPIO_Config(void)   //  蜂鸣器引脚初始化 PC14    实际需要接3.3V
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE); // ??PC????
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;  //???????
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);  //???PC??
}

int main()
{
  u16 i;
  u32 temp;
  u32 adc_num;
  SystemInit(); // 配置系统时钟为72M
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);       //使能AFIO
  GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable , ENABLE);   //完全禁用JTAG

  delay_init();               //初始化   引脚
  LED_GPIO_Config();
  Buzzer_GPIO_Config();
  USART1_Config(115200);
  USART2_Config(115200);    //接esp8266
	DS18B20_Init();

	DS18B20_GetTemperature();  //获取温度  前一两次得到是85  舍弃
	
  Adc_Init();           //读取甲烷模块的  初始化ADC
  GPIO_Configuration();       //lcd1602引脚初始化
  LCD1602_Init();                    //lcd1602模式初始化
  LCD1602_Show_Str(0, 0, "     welcome    ");  //第0行第一列    lcd1602显示
  LCD1602_Show_Str(0, 1, "connect to wifi.");
  LED = 0;
  BUZZER = 0;    //开机 亮灯  鸣叫
  delay_ms(100);
	DS18B20_GetTemperature();  //获取温度  前一两次得到是85  舍弃
  USART_Send_string(USART2, "AT+CWMODE=1\r\n");    //设置为STA模式     wifi模块的
  delay_ms(20);
  LED = 1;
  BUZZER = 1;	  //关灯  不响
  //USART_Send_string(USART2,"AT+CWJAP=\"Tenda_8506D0\",\"td123456.\"\r\n");  //
  USART_Send_string(USART2, "AT+CWJAP=\"Honor8\",\"ly990204\"\r\n");       //设置esp8266连接的wifi名和密码
  delay_ms(20);
  USART_Send_string(USART2, "AT+RST\r\n"); //wifi模块重启

  LCD1602_Show_Str(0, 0, "The port is 8080");  //第0行第一列
  LCD1602_Show_Str(0, 1, "Get IP.....wait");     //显示正在获取IP
  delay_s(10);
  USART_Send_string(USART2, "AT+CIPMODE=0\r\n");      //关闭透传模式
  delay_ms(20);
  USART_Send_string(USART2, "AT+CIPMUX=1\r\n");        //开启多连接模式
  delay_ms(20);
  USART_Send_string(USART2, "AT+CIPSERVER=1,8080\r\n");    //开启服务器  端口8080
  get_ip_flag = 1;
  delay_ms(20);
  USART_Send_string(USART2, "AT+CIFSR\r\n");    //获取wifi模块的IP地址
  delay_ms(30);
  get_ip_flag = 0;

  USART_Send_string(USART1, "\r\nGET IP address:[");
  USART_Send_string(USART1, IP_str);             //串口1发送  IP信息
  USART_Send_string(USART1, "]end\r\n");

  for (i = 0; i < 16; i++) {   //字符串结尾
    if (IP_str[i] == '\"') {
      IP_str[i] = '\0';
      break;
    }
  }
  LCD1602_Show_Str(0, 1, IP_str);           //在lcd1602上显示IP地址
  LCD1602_Show_Str(i, 1, "      ");

  delay_s(10);
  while (1)
  {
    delay_ms(200);    //自带的灯闪烁
    LED0 = 1;
    delay_ms(500);
    LED0 = 0;

		temp = DS18B20_GetTemperature() * 100; //获取温度值  100倍
    LCD1602_Show_Str(0, 0, "temp:");   //显示在1602
    LCD1602_Set_Cursor(5, 0);    //坐标
    LCD1602_Write_Dat(temp / 1000 % 10 + 48);
    LCD1602_Write_Dat(temp / 100 % 10 + 48);
    LCD1602_Write_Dat('.');
    LCD1602_Write_Dat(temp / 10 % 10 + 48);
    LCD1602_Write_Dat(temp % 10 + 48);
    LCD1602_Write_Dat('C');
    LCD1602_Show_Str(11, 0, "     ");

    adc_num = (Get_Adc(ADC_CH0) - 800) / 4096.0 * 100000; //数值的10倍   ppm  大概的校准了 -800   转化成0-10000ppm
    LCD1602_Show_Str(0, 1, "CH4:");    //显示在1602上
    LCD1602_Set_Cursor(4, 1);    //坐标
    LCD1602_Write_Dat(adc_num / 10000 % 10 + 48);
    LCD1602_Write_Dat(adc_num / 1000 % 10 + 48);
    LCD1602_Write_Dat(adc_num / 100 % 10 + 48);
    LCD1602_Write_Dat(adc_num / 10 % 10 + 48);
    LCD1602_Write_Dat('.');
    LCD1602_Write_Dat(adc_num % 10 + 48);
    LCD1602_Write_Dat('p');
    LCD1602_Write_Dat('p');
    LCD1602_Write_Dat('m');
    LCD1602_Show_Str(13, 1, "   ");
    delay_ms(300);
		
    USART_Send_string(USART2, "AT+CIPSEND=0,28\r\n");   //通知wifi模块 发送给客户端  数据
    delay_ms(6);
    i = temp;
    USART_Send_string(USART2, "temp:");      //发送的数据
    USART_Send_Char(USART2, i / 1000 % 10 + 48);
    USART_Send_Char(USART2, i / 100 % 10 + 48);
    USART_Send_Char(USART2, '.');
    USART_Send_Char(USART2, i / 10 % 10 + 48);
    USART_Send_Char(USART2, i % 10 + 48);
    USART_Send_Char(USART2, 'C');

    i = adc_num;
    USART_Send_string(USART2, " ;  CH4:");
    USART_Send_Char(USART2, i / 10000 % 10 + 48);
    USART_Send_Char(USART2, i / 1000 % 10 + 48);
    USART_Send_Char(USART2, i / 100 % 10 + 48);
    USART_Send_Char(USART2, i / 10 % 10 + 48);
    USART_Send_Char(USART2, '.');
    USART_Send_Char(USART2, i % 10 + 48);
    USART_Send_string(USART2, "mmp");



    if (temp >= 30*100) { //温度大于30  亮灯
      LED = 0;
    }
    if (temp >= 32*100) { //温度大于32，鸣叫
      BUZZER = 0;
      LCD1602_Show_Str(0, 0, "temp warming!");   //显示在1602
      delay_ms(10);
      USART_Send_string(USART2, "AT+CIPSEND=0,17\r\n");     //发送给手机
      delay_ms(6);
      USART_Send_string(USART2, "temp>32C warming!   ");
    }

    if (adc_num >= 1000 * 10) { //甲烷  大于1000ppm  亮灯
      LED = 0;
    }
    if (adc_num >= 2000 * 10) { //大于2000ppm  鸣叫
      BUZZER = 0;
      LCD1602_Show_Str(0, 1, "CH4 warming!  ");    //显示在1602
      delay_ms(10);
      USART_Send_string(USART2, "AT+CIPSEND=0,19\r\n");      //发送给手机
      delay_ms(6);
      USART_Send_string(USART2, "CH4>2000mmp warming!    ");
    }

    if ((temp < 3000) && (adc_num < 10000))  LED = 1; //都正常  灯灭
    if ((temp < 3200) && (adc_num < 20000))  BUZZER = 1; //关闭  蜂鸣器
  }

}

void USART2_IRQHandler(void)            //串口2 中断服务函数   连接esp8266的
{
  u8 dat;
  if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)   //?????????
  {
    dat = USART_ReceiveData(USART2);    //接收wifi发回的数据

    if (get_ip_flag == 1) { //是 要获取ip标志
      if (IP_num < 15) { //
        if (dat == '\r')  {
          IP_num = 20;  //当得到的是回车符  是ip数据的最后了  完成接收
          get_ip_flag = 0;
        }
        IP_str[IP_num] = dat;
        IP_num++;
      }
      if (dat == '\"')    IP_num = 0; //从接收到 引号 开始存储ip地址数据
    }

    //USART_Send_Char(USART1, dat);    //将wifi发回的数据转发给串口1

  }
}
